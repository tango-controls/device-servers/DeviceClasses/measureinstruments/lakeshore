#!/usr/bin/env python

import sys
import tango


def main():
    try:
        py = tango.Util(sys.argv)
        module_name, server_type = __name__.split('.')
        server_name = server_type + '/' + sys.argv[1]
        db = tango.Database()
        classes = db.get_server_class_list(server_name)
        for tg_class_name in classes.value_string:
            module_name = module_name + '.' + tg_class_name
            module = __import__(
                module_name, fromlist=[tg_class_name, tg_class_name + 'Class'])
            impl_class = getattr(module, tg_class_name)
            dev_class = getattr(module, tg_class_name + 'Class')
            py.add_class(dev_class, impl_class)
        U = tango.Util.instance()
        U.server_init()
        U.server_run()

    except tango.DevFailed as e:
        print('-------> Received a DevFailed exception:', str(e))
    except Exception as e:
        print('-------> An unforeseen exception occurred....', str(e))


if __name__ == '__main__':
    main()
