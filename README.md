Tango device servers for lakeshore instruments.

Requires lakeshore libraries. See:
    https://github.com/lakeshorecryotronics/python-driver

This tango device server requires that the servers are defined in a tango database. The server will analyze the defined classes for each instance and will automatically add those to the server (and only those).

To add a new instrument you just have to:
- define its interface in a file named exactly as the tango class it will implement.
- define a server instance with a class of your instrument's type
